import Hero from './Components/Hero';
import Work from './Components/Work';
import Footer from './Components/Footer';
import './App.css';
import React from 'react';

function App() {
  return (
    <div className="app-wrapper">
      <Hero />
      <Work />
      <Footer />
    </div>
  );
}

export default App;
