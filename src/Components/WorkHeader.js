function WorkHeader(props) {

    return(
        <div className="work-header">
            <div className="data-row">
                <ul>
                    <li className="year-header">Year</li>
                    <li className="agency-header">Agency</li>
                    <li className="client-header">Client</li>
                    <li className="project-header">Project Type</li>
                    <li className="role-header">My Role</li>
                    {/* <li className="desc-header">Description</li> */}
                </ul>
            </div>
        </div>
    )
}

  export default WorkHeader