function WorkItem(props) {

    return(
        <div className="work-item">
            <div className="data-row">
                <ul>
                    <li className="year">{props.year}</li>
                    <li className="agency">{props.agency}</li>
                    <li className="client">{props.client}</li>
                    <li className="project">{props.project}</li>
                    <li className="role">{props.role}</li>
                    <li className="desc">{props.details}</li>
                </ul>
            </div>
        </div>

    )
        
  }

  export default WorkItem