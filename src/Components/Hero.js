function Hero() {

    return(
        <div className="hero">
            {/* <a href="https://fcatalyst.com/overview">@FCAT</a> */}
            <h1>Mike Griffin <span className="pipe">|</span> UX Designer</h1>
            <p>Currently a senior UX designer <a href="https://twitter.com/xwerx">@Xwerx</a> - I'm a dad of two, lover of good design, hobbyist developer, Jiu-jitsu practitioner and tech enthusiast.</p>
        </div>
    )
        
  }

  export default Hero