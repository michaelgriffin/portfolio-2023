function Footer() {

    return(
        <div className="footer">
            <p>Designed & Developed by me. <a href="https://twitter.com/MikeGriffin87">Twitter</a> | <a href="https://www.linkedin.com/in/mike-griffin-67697473/">LinkedIn</a></p>
        </div>
    )
        
  }

  export default Footer