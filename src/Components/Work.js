import WorkItem from "./WorkItem"
import WorkHeader from "./WorkHeader"
import Data from "../Assets/Data/data.json"
import React from "react"


function MyWork() {

    return(
        <div className="work-section">
            <WorkHeader />
            {Data.map((item) => (
            <WorkItem
                year={item.year}
                agency={item.agency}
                client={item.client}
                project={item.project}
                role={item.role}
                details={item.deets}
            />
        ))}
        </div>
    )}

  export default MyWork